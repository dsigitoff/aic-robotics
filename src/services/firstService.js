export default class firstService {
    _apiBase = 'http://api.mrthefirst.pro/pairs-list/'

    async getResource() {
        const res = await fetch(`${this._apiBase}`);

        if (!res.ok) {
            throw new Error(`Can't fetch ${this._apiBase}, received ${res.status}`)
        }

        return await res.json();
    }
}
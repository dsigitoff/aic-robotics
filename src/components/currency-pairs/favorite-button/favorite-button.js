import React, {Fragment} from 'react'

import './favorite-button.css'

const FavoriteButton = () => {
    return (
        <Fragment>
            <img className="star_favorites--m"
                src={"/assets/star_favorites2.svg"}
                 alt="star"
            />
        </Fragment>
    )
};

export default FavoriteButton
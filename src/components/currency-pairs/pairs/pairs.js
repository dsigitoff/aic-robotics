import React from 'react'

import './pairs.css'

const Pairs = (props) => {
    return (
        <div className="pairs--mr">
            <span className="first-pair">{props.firstPair ? props.firstPair : 'xnt'}</span>
            <span className="second-pair">/</span>
            <span className="second-pair second-pair--border">{props.secondPair ? props.secondPair : 'btc'}</span>
        </div>
    )
};

export default Pairs
import React from 'react'

import './popular-list.css'

const PopularList = (props) => {
    const currencyArr = ['btc', 'xnt', 'btc', 'ert', 'ltc'];

    return (
        <ul className="currency-pairs__popular-list currency-pairs__popular-list--mr currency-pairs__popular-list--border">
            {currencyArr.map((item, index) =>
                <li
                    key={index}
                    onClick={() => props.updatePopularList(item)}
                    className="popular-list__item"
                >{item}</li>)}
        </ul>
    )
};

export default PopularList
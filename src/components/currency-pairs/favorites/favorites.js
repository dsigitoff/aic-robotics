import React from 'react'

import './favorites.css'

import FavoriteButton from '../favorite-button'

const Favorites = () => {
  return (
      <div className="currency-pairs__favorites">
          <FavoriteButton/>
          <span>Favorites</span>
      </div>
  )
};

export default Favorites
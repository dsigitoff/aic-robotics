import React, {Component} from 'react';

import './input-search.css'
import FirstService from "../../../services/firstService";

export default class InputSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listItems: []
        };
        this.onPageLoaded = this.onPageLoaded.bind(this);
    }

    firstService = new FirstService();

    componentDidMount() {
        this.onPageLoaded()
    }

    onPageLoaded() {
        this.firstService
            .getResource()
            .then(res => {
                this.setState({
                    listItems: res
                })
            });
    }

    render() {
        const optionsList = this.state.listItems.map((item, index) =>
            <option key={index}>{item.currency_codes[0] + '/' + item.currency_codes[1]}</option>);

        return (
            <div>
                <input className="currency-pairs__input" type="text" list="currency-pairs"/>
                <datalist id="currency-pairs">
                    {optionsList}
                </datalist>
            </div>
        )
    }
}
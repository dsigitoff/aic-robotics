import React, {Component} from 'react'

import Pairs from './pairs'
import InputSearch from './input-search'
import Favorites from './favorites'
import PopularList from './popular-list'
import ListItem from "./list-item";

import './currency-pairs.css'

export default class CurrencyPairs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstPair: '',
            secondPair: '',
            favoriteClicked: false,
            popularPair: ''
        };
        this.handleFavorite = this.handleFavorite.bind(this)
    }

    updateData = (firstPair, secondPair) => {
        this.setState({
            firstPair,
            secondPair
        })
    };

    updatePopularList = (popularPair) => {
        this.setState({
            popularPair
        })
    };

    handleFavorite() {
        this.setState({
            favoriteClicked: !this.state.favoriteClicked
        })
    }

    render() {
        const {firstPair, secondPair, favoriteClicked, popularPair} = this.state;

        return (
            <section className="currency-pairs">
                <header className="currency--ml">
                    <h5 className="currency-title">currency pairs</h5>
                </header>
                <div className="currency-search currency--ml currency-search--border">
                    <Pairs
                        firstPair={firstPair}
                        secondPair={secondPair}
                    />
                    <InputSearch/>
                </div>
                <div className="currency-pairs__filters currency--ptb">
                    <div onClick={this.handleFavorite}><Favorites/></div>
                    <PopularList
                        updatePopularList={this.updatePopularList}
                    />
                </div>
                <div className="currency-pairs__list">
                    <ul className="currency-pairs__titles">
                        <li><img className="star_favorites--m" src="/assets/star_favorites.svg" alt="star"/></li>
                        <li>market</li>
                        <li>price</li>
                        <li>vol</li>
                        <li>+/-</li>
                    </ul>
                    <ListItem
                        updateData={this.updateData}
                        activateFavorites={favoriteClicked}
                        popularPair={popularPair}
                    />
                </div>
            </section>
        )
    }
}
import React, {Component, Fragment} from 'react'

import FirstService from '../../../services/firstService'

import FavoriteButton from '../favorite-button'

import './list-item.css'

export default class ListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listItems: [],
            favoriteList: [],
            clicked: false,
            firstPair: ''
        };
        this.onPageLoaded = this.onPageLoaded.bind(this);
        this.pageRefresher = this.pageRefresher.bind(this);
        this.handleStarClick = this.handleStarClick.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
    }

    firstService = new FirstService();

    componentDidMount() {
        this.onPageLoaded()
    }

    handleItemClick(e) {
        const {listItems} = this.state;
        const firstPair = listItems[e.currentTarget.id].currency_codes[0];
        const secondPair = listItems[e.currentTarget.id].currency_codes[1];

        this.setState({
            firstPair,
            secondPair
        });

        this.props.updateData(firstPair, secondPair);
    }

    handleStarClick(e) {
        const id = e.currentTarget.id;
        const {favoriteList} = this.state;
        const {listItems} = this.state;

        this.setState({
            favoriteList: [...favoriteList, listItems[id]]
        });

        e.target.src = e.target.src === "http://localhost:3000/assets/star_favorites2.svg"
            ? "http://localhost:3000/assets/star_favorites.svg"
            : "http://localhost:3000/assets/star_favorites2.svg";
    }

    onPageLoaded() {
        this.firstService
            .getResource()
            .then(res => {
                this.setState({
                    listItems: res
                })
            });
        this.pageRefresher()
    }

    pageRefresher() {
        setTimeout(() => this.onPageLoaded(), 10000)
    }

    render() {
        const {popularPair, activateFavorites} = this.props;
        const items = this.state.listItems;
        const favorites = this.state.favoriteList;
        const popularItems = items.filter(item => item.currency_codes[1] === popularPair.toUpperCase());

        const popularList = popularItems.map((item, index) =>
            <ul
                key={index}
                id={index}
                className={index % 2 !== 0 ? "currency-list list-item--odd" : "currency-list"}
                onClick={this.handleItemClick}
            >
                <li
                    className="list-item"
                    onClick={this.handleStarClick}
                    id={index}
                >
                    <FavoriteButton
                    />
                </li>
                <li className="list-item">{item.currency_codes[0]}</li>
                <li className="list-item">{item.price}</li>
                <li className="list-item">{item.vol}</li>
                <li className="list-item last-li--fz">{item.change > 0 ?
                    <span className="list-item--green">{item.change}</span> :
                    <span className="list-item--red">{item.change}</span>}%
                </li>
            </ul>);

        const favoriteList = favorites.map((item, index) =>
            <ul
                key={index}
                id={index}
                className={index % 2 !== 0 ? "currency-list list-item--odd" : "currency-list"}
                onClick={this.handleItemClick}
            >
                <li
                    className="list-item"
                    onClick={this.handleStarClick}
                    id={index}
                >
                    <FavoriteButton
                    />
                </li>
                <li className="list-item">{item.currency_codes[0]}</li>
                <li className="list-item">{item.price}</li>
                <li className="list-item">{item.vol}</li>
                <li className="list-item last-li--fz">{item.change > 0 ?
                    <span className="list-item--green">{item.change}</span> :
                    <span className="list-item--red">{item.change}</span>}%
                </li>
            </ul>);

        const listItems = items ? items.map((item, index) =>
            <ul
                key={index}
                id={index}
                className={index % 2 !== 0 ? "currency-list list-item--odd" : "currency-list"}
                onClick={this.handleItemClick}
            >
                <li
                    className="list-item"
                    onClick={this.handleStarClick}
                    id={index}
                >
                    <FavoriteButton/>
                </li>
                <li className="list-item">{item.currency_codes[0]}</li>
                <li className="list-item">{item.price}</li>
                <li className="list-item">{item.vol}</li>
                <li className="list-item last-li--fz">{item.change > 0 ?
                    <span className="list-item--green">{item.change}</span> :
                    <span className="list-item--red">{item.change}</span>}%
                </li>
            </ul>) : 'loading...';

        return (
            <Fragment>{activateFavorites ? favoriteList : popularPair ? popularList : listItems}</Fragment>
        )
    }
};

import React from 'react'

import CurrencyPairs from '../currency-pairs'

import './app.css'

const App = () => {
    return (
        <section className="app app--padding">
            <CurrencyPairs/>
        </section>
    )
};

export default App